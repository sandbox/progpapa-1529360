<?php

/**
 * @file
 * Iconfonts is an API module to enable easy integration of icon fonts with the
 * font-your-face module.
 */

/**
 * Implements hook_menu().
 */
function iconfonts_menu() {

  $items = array();

  $items['admin/iconfonts/%iconfonts_char/edit'] = array(
    'title' => 'Edit character',
    'description' => 'Edit charater',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('iconfonts_char_edit_form', 2),
    'access arguments' => array('administer @font-your-face'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'iconfonts.admin.inc',
  );

  return $items;

}

/**
 * Loads a character based on its character id.
 *
 * @param int $cid
 *   The id of the character to be loaded.
 *
 * @return object
 *   The loaded character object.
 */
function iconfonts_char_load($cid) {

  // Needs caching as loader function is called multiple times.
  // see http://drupal.org/node/1471554
  static $char = FALSE;

  if (!$char) {
    $char = iconfonts_get_char($cid);
    return $char;
  }

}

/**
 * Implements hook_fontyourface_info().
 */
function iconfonts_fontyourface_info() {

  $info = array(
    'name' => 'Icon Fonts',
    // to get rid of PHP notices
    // @todo: change this to the module url?
    'url' => 'http://drupal.org',
    'base_path' => 'http://drupal.org',
  );

  return $info;

}

/**
 * Implements hook_fontyourface_import().
 */
function iconfonts_fontyourface_import() {

  // Calls hook_iconfonts_import().
  foreach (module_implements('iconfonts_import') as $module) {

    // Don't run import on already imported fonts.
    if (!iconfonts_get_fid_by_module_name($module)) {

      $function = $module . '_iconfonts_import';

      $font_import = $function();

      $font = new StdClass();
      $font->name = $font_import['name'];
      $font->url = $font_import['url'];
      $font->css_family = $font_import['family'];
      // All iconfonts will have the same provider this way all icon fonts will
      // be listed under the same tab at '/appearance/fontyourface/browse'. It
      // means that fontyourface_provider_disable() can't be used on iconfonts.
      $font->provider = 'iconfonts';

      // Stores the name of the module providing the icons in the metadata. This
      // is going to be used in several occasions, like
      // fontyourface_provider_disable() or iconfonts_fontyourface_enable().
      $metadata['module'] = $module;
      $font->metadata = serialize($metadata);

      fontyourface_save_font($font);

      iconfonts_enable_font($font);

    }

  }

  return TRUE;

}

/**
 * Populates the database with font information.
 *
 * @param object $font
 *   A font object.
 *
 * @return bool
 *   A boolean indicating whether the import was successful or not.
 */
function iconfonts_enable_font($font) {

  $success = TRUE;

  // Gets the name of the module providing the icon font then calls
  // hook_iconfonts_enable($font).
  $metadata = unserialize($font->metadata);
  $module = $metadata['module'];
  $function = $module . '_iconfonts_enable';

  if (function_exists($function)) {

    $char_info = $function($font);
    // Populates the iconfonts_chars database table.
    iconfonts_save_chars($char_info, $font->fid);

  }

  else {

    drupal_set_message(t('No character info for @fontname.', array('@fontname' => $font->name)), 'warning');
    $success = FALSE;

  }

  return $success;

}

/**
 * Implements hook_form_FORM_ID_alter() for
 * fontyourface_ui_apply_by_font_form().
 *
 * Removes the 'css selector' select list from the 'By Font' tab of the
 * admin/appearance/fontyourface page and displays a message that css
 * selectors are available on the character level.
 */
function iconfonts_form_fontyourface_ui_apply_by_font_form_alter(&$form, &$form_state, $form_id) {

  $iconfonts = iconfonts_get_enabled_fonts();

  foreach ($iconfonts as $fid => $name) {

      $form['table']['row_' . $fid]['css_selector[' . $fid . ']']['#type']
        = 'markup';
      $form['table']['row_' . $fid]['css_selector[' . $fid . ']']['#markup']
        = t("Click 'Edit' to add css selectors on the character level");

  }

}

/**
 * Implements hook_form_FORM_ID_alter() for fontyourface_ui_apply_by_selector_form().
 *
 * Removes all iconfonts from the 'By CSS Selector' tab of the
 * admin/appearance/fontyourface' page.
 */
function iconfonts_form_fontyourface_ui_apply_by_selector_form_alter(&$form, &$form_state, $form_id) {

  $iconfonts = iconfonts_get_enabled_fonts();

  $selectors = element_children($form['table']);

  foreach ($selectors as $selector) {

    $selector_id = substr($selector, 4);
    $options = $form['table'][$selector]['fid[' . $selector_id . ']']['#options'];
    $form['table'][$selector]['fid[' . $selector_id . ']']['#options'] = array_diff($options, $iconfonts);

  }

}

/**
 * Implements hook_form_FORM_ID_alter() for fontyourface_ui_admin_edit_form().
 *
 * Adds list of icons to the font edit form.
 */
function iconfonts_form_fontyourface_ui_admin_edit_form_alter(&$form, &$form_state, $form_id) {

  // Modifies the form only if the provider is set to 'Icon Fonts'.
  if (strpos($form['details']['provider']['#markup'], 'Icon Fonts')) {

    $font = fontyourface_get_font($form['fid']['#value']);
    iconfonts_font_registry($font);

    $form['iconfonts_chars'] = array(
      '#type' => 'fieldset',
      '#title' => t('Characters'),
      '#description' => t('Click on an icon to add/remove css selectors or
                          enable/disable it.'),
      '#weight' => -1,
    );

    $character_list = iconfonts_get_chars_by_fid($form['fid']['#value'], 'all', TRUE);
    $enabled['characters'] = $character_list['enabled'];
    $enabled['enabled'] = TRUE;
    $disabled['characters'] = $character_list['disabled'];
    $disabled['enabled'] = FALSE;

    $form['iconfonts_chars']['enabled'] = array(
      '#type' => 'item',
      '#markup' => theme('iconfonts_char_list', $enabled),
    );

    $form['iconfonts_chars']['disabled'] = array(
      '#type' => 'item',
      '#markup' => theme('iconfonts_char_list', $disabled),
    );

    // Disables the 'Preview' fieldset, not used by the iconfonts module.
    unset($form['preview']);

    // Hides the 'CSS selector' textarea. Cannot unset it, since
    // fontyourface_ui_admin_edit_form_submit() throws a php notice.
    $form['css']['#access'] = FALSE;

    $form['details']['#collapsible'] = FALSE;
    $form['details']['#weight'] = -50;

    $form['#submit'][] = 'iconfonts_font_edit_form_submit';

    $form['#attached']['css'][]
      = drupal_get_path('module', 'iconfonts') . '/iconfonts-admin.css';

  }

}

/**
 * Form submission handler for fontyourface_ui_admin_edit_form().
 *
 * Makes sure the user is always redirected, even if the 'destination'
 * parameter is not set.
 */
function iconfonts_font_edit_form_submit($form, &$form_state) {

  drupal_goto('admin/appearance/fontyourface');

}

/**
 * Implements hook_theme().
 */
function iconfonts_theme($existing, $type, $theme, $path) {

  return array(
    'iconfonts_char_list' => array(
      'variables' => array(
        'characters' => NULL,
        'enabled' => NULL,
      ),
      'file' => 'iconfonts.theme.inc',
    ),
  );

}

/**
 * Implements hook_preprocess_HOOK() for html.tpl.php.
 */
function iconfonts_preprocess_html(&$vars) {

  $fonts = iconfonts_font_registry() + iconfonts_get_fonts();

  $css = iconfonts_generate_css($fonts);

  $css_md5 = md5($css);

  if ($css_md5 != variable_get('iconfonts_css_md5', '')) {

    iconfonts_rewrite_css($css);
    variable_set('iconfonts_css_md5', $css_md5);

  }

  if ($css != '') {
    fontyourface_add_css_in_preprocess($vars, 'fontyourface/iconfonts.css');
  }

  $vars['iconfonts'] = $fonts;

}

/**
 * Manages registry of icon fonts used on the current page.
 *
 * Need to use a separate function instead of fontyourface_font_registry() so
 * that there will be no regular fonts in the return value. Otherwise $fonts in
 * iconfonts_preprocess_html() will include regular fonts, which causes problems
 * or additional checks when writing the css file.
 *
 * @param object|bool $font
 *   A font object, defaults to FALSE.
 * @param bool $clear
 *   Whether to clear the statice variable or not.
 *
 * @return array
 *   An associative array containing the font objects, keyed by the font ids.
 */
function iconfonts_font_registry($font = FALSE, $clear = FALSE) {

  static $fonts = array();

  if ($clear) {
    $fonts = array();
  }

  if ($font) {
    $fonts[$font->fid] = $font;
  }

  return $fonts;

}

/**
 * Get iconfonts. Defaults to all enabled.
 *
 * @param string $where
 *   Will be inserted into the SQL query.
 * @param string $order_by
 *   Will be inserted into the SQL query.
 */
function iconfonts_get_fonts($where = "enabled = 1 AND provider = 'iconfonts'", $order_by = 'name ASC') {

  return fontyourface_get_fonts($where, $order_by);

}

/**
 * Generates CSS.
 *
 * @param array $fonts
 *   An array of font objects.
 *
 * @return string
 *   A group of css rules.
 */
function iconfonts_generate_css($fonts = array()) {

  // Get font list.
  if (empty($fonts)) {

    $fonts = iconfonts_font_registry() + iconfonts_get_fonts();

  }

  // Generate CSS.
  $css = '';

  foreach ($fonts as $font) {

    $chars = iconfonts_get_chars_by_fid($font->fid, 'enabled');

    foreach ($chars['enabled'] as $char) {
      $css_selector = str_replace(',', ':before,', $char->css_selector) .
        ':before ';
      $css .= $css_selector . '{ content: "' . $char->code .
        '"; font-family: "' . $font->css_family . '"; }' . "\n";
    }

  }

  $css .= iconfonts_generate_css_for_chars();

  return $css;

}// iconfonts_generate_css

/**
 * Re-writes iconfonts.css file.
 *
 * @param string $css
 *   A group of css rules to insert into iconfonts.css.
 */
function iconfonts_rewrite_css($css) {

  // Write CSS.
  $destination =
    file_stream_wrapper_uri_normalize('public://fontyourface/iconfonts.css');
  $destination_directory = dirname($destination);

  if (file_prepare_directory($destination_directory, FILE_CREATE_DIRECTORY)) {
    file_unmanaged_save_data($css, $destination, FILE_EXISTS_REPLACE);
  }

}

/**
 * Generates the content of the css file.
 *
 * @return string
 *   The content of the css file.
 */
function iconfonts_generate_css_for_chars() {

  $chars = iconfonts_char_registry();
  $css = '';

  if (!empty($chars)) {
    foreach ($chars as $char) {

      $css_selector = '.' . $char->name . ':before ';
      $css .= $css_selector . '{ content: "' . $char->code .
        '"; font-family: "' . $char->css_family . '"; }' . "\n";

    }

  }

  return $css;

}

/**
 * Manages registry of characters used on current page, to provide list to
 * iconfonts_generate_css_for_chars().
 *
 * @param object|bool $char
 *   A character object. Defaults to FALSE.
 * @param bool $clear
 *   Whether to clear the statice variable or not.
 *
 * @return array
 *   An associative array containing the character objects, keyed by the
 *   character ids. Characters have the css_family extra property.
 */
function iconfonts_char_registry($char = FALSE, $clear = FALSE) {

  static $chars = array();
  static $fonts = array();

  if ($clear) {
    $chars = array();
  }

  if ($char) {
    if (!isset($fonts[$char->fid])) {
      $font = fontyourface_get_font($char->fid, TRUE);
      $fonts[$char->fid] = $font->css_family;
    }
    // Adds the font css family to the character, so that it can be used in
    // iconfonts_generate_css_for_chars() to build the iconfonts.css file.
    $char->css_family = $fonts[$char->fid];
    $chars[$char->cid] = $char;
  }
  return $chars;

}

/**
 * Loads a character based on its character id.
 *
 * @param int $cid
 *    The character id.
 *
 * @return object
 *    The complete character object.
 */
function iconfonts_get_char($cid) {

  return db_query('SELECT * FROM {iconfonts_chars} WHERE cid = :cid', array(':cid' => $cid))->fetchObject();

}

/**
 * Get the module name by providing a font id.
 *
 * Since the provider of all iconfonts are set to 'iconfont',
 * an extra step is needed to find out which module provides the font.
 *
 * @param int $fid
 *    The font id of the icon font.
 *
 * @return string
 *    The name of the module that provides the font.
 */
function iconfonts_get_module_name_by_fid($fid) {

  $result = db_query("SELECT metadata FROM {fontyourface_font}
                     WHERE fid = :fid", array(':fid' => $fid))->fetchObject();

  $metadata = unserialize($result->metadata);

  return $metadata['module'];

}

/**
 * Enables a single character.
 *
 * @param object $char
 *    A character object.
 */
function iconfonts_enable_char($char) {

  db_update('iconfonts_chars')
    ->fields(array('enabled' => 1))
    ->condition('cid', $char->cid)
    ->execute();

  return TRUE;

}

/**
 * Disables a single character.
 *
 * @param object $char
 *    A character object.
 */
function iconfonts_disable_char($char) {

  db_update('iconfonts_chars')
    ->fields(array('enabled' => 0))
    ->condition('cid', $char->cid)
    ->execute();

  return TRUE;

}

/**
 * Sets a character's CSS selector.
 *
 * @param object $char
 *    A character object.
 * @param string $css_selector
 *    Comma separated list of css selectors.
 */
function iconfonts_set_css_selector($char, $css_selector) {

  db_update('iconfonts_chars')
    ->fields(array('css_selector' => $css_selector))
    ->condition('cid', $char->cid)
    ->execute();

}

/**
 * Saves character information into the database.
 *
 * @param array $char_info
 *   An array of character information arrays. Each array contains:
 *   - name: The name of the character.
 *   - code: The code of the character.
 *   - css_selector: (optional) The default css selector of the character.
 *   - enabled: (optional) The default enabled value of the character.
 * @param int $fid
 *    Font id.
 */
function iconfonts_save_chars($char_info, $fid) {

  $query = db_insert('iconfonts_chars')
    ->fields(array('fid', 'name', 'code', 'css_selector', 'enabled'));

  foreach ($char_info as $char) {
    $char['fid'] = $fid;

    if (!isset($char['css_selector'])) {
      // Defaults to no css selector.
      $char['css_selector'] = '';
    }

    if (!isset($char['enabled'])) {
      // Defaults to disabled character.
      $char['enabled'] = 0;
    }


    $query->values($char);

  }

  $query->execute();

}

/**
 * Returns the list of characters of the font defined by $fid.
 *
 * @param int $fid
 *   The font id.
 * @param string $status
 *   'enabled' or'disabled' depending on the status of the icons to retrun.
 *   Defaults to return both enabled and disabled icons.
 * @param bool $register
 *   If TRUE, iconfonts_char_registry() is called to register an icon. Defaults
 *   to FALSE.
 *
 * @return array
 *   An associative array containing:
 *   - enabled: An associative array of enabled character objects keyed by the
 *     character id.
 *   - disabled: An associative array of disabled character objects keyed by
 *     the character id.
 */
function iconfonts_get_chars_by_fid($fid, $status = 'all', $register = FALSE) {

  $chars = array();
  $chars['enabled'] = array();
  $chars['disabled'] = array();

  $font = fontyourface_get_font($fid);

  $font_type = iconfonts_get_module_name_by_fid($fid);

  $query = db_select('iconfonts_chars', 'i');
  $query
    ->fields('i')
    ->condition('i.fid', $fid);

  if ($status == 'enabled') {
    $query->condition('i.enabled', 1);
  }
  elseif ($status == 'disabled') {
    $query->condition('i.enabled', 0);
  }

  // Lets submodules modify the query.
  $query->addTag('iconfonts_characters')
    ->addMetaData('font_type', $font_type);

  $result = $query->execute();

  foreach ($result as $char) {

    if ($char->enabled) {
      $chars['enabled'][$char->cid] = $char;
    }
    else {
      $chars['disabled'][$char->cid] = $char;
    }

    if ($register) {
      iconfonts_char_registry($char);
    }

  }

  return $chars;

}

/**
 * Gets the font id from a module's name.
 *
 * @param string $module
 *   The name of the module.
 *
 * @return int|bool
 *   The font id. Returns FALSE if the font is not found.
 */
function iconfonts_get_fid_by_module_name($module) {

  $fonts = db_query("SELECT fid, metadata FROM {fontyourface_font}
                    WHERE provider = 'iconfonts'");

  foreach ($fonts as $font) {

    $metadata = unserialize($font->metadata);
    if ($metadata['module'] == $module) {
      return (int) $font->fid;
    }

  }

  return FALSE;

}

/**
 * Removes all font information from the database.
 *
 * @param string $module
 *   The name of the module.
 */
function iconfonts_uninstall_font($module) {

  $fid = iconfonts_get_fid_by_module_name($module);

  db_delete('fontyourface_font')
    ->condition('fid', $fid)
    ->execute();

  // @todo: delete tags??

  db_delete('iconfonts_chars')
    ->condition('fid', $fid)
    ->execute();

}

/**
 * Gets css family of a font.
 *
 * @param int $fid
 *   Font id.
 *
 * @return string
 *   The css family of the font.
 */
function iconfonts_get_css_family($fid) {

  $ret = db_query('SELECT css_family FROM {fontyourface_font} WHERE fid = :fid', array(':fid' => $fid))->fetchObject();
  return $ret->css_family;

}

/**
 * Gets all enabled iconfonts.
 *
 * @return array
 *   An associative array, where the keys are the font ids and
 * the values are the font names.
 */
function iconfonts_get_enabled_fonts() {

  $fonts = db_query("SELECT fid, name FROM {fontyourface_font} WHERE enabled = 1 AND provider = 'iconfonts'")->fetchAllKeyed();

  return $fonts;

}

/**
 * Implements hook_fontyourface_short_preview_text().
 *
 * Displays a few random icons on the browse screen.
 */
function iconfonts_fontyourface_short_preview_text($font) {

  // Makes sure the appropriate css file is loaded, even for disabled fonts.
  iconfonts_font_registry($font);

  drupal_add_css(drupal_get_path('module', 'iconfonts') . '/iconfonts-admin.css');

  $chars = db_select('iconfonts_chars', 'i')
    ->condition('i.fid', $font->fid, '=')
    ->fields('i')
    ->orderRandom()
    ->range(0, 5)
    ->execute()
    ->fetchAllAssoc('name');

  $ret = '';

  foreach ($chars as $name => $char) {

    iconfonts_char_registry($char);

    $ret .= '<span class="' . $name . ' iconfonts-preview"></span>';

  }

  return $ret;

}
